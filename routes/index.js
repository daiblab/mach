var express = require('express');
var router = express.Router();
let db = require('../utils/db');
var machUsers = require('../services/users');
var machEthers = require('../services/ethers');
var datetime = require('node-datetime');

var sessionChecker = (req, res, next) => {
    console.log('sessionID =>', req.sessionID)
    console.log('session.userTag =>', req.session.userTag)
    console.log('session.userId =>', req.session.userId)

    if (req.session.userTag && req.cookies.bitweb_sid) {
        next();
    } else {
        res.redirect('/login');
    }
};

router.get('/', sessionChecker, function(req, res, next) {
    res.render('login/login', { title: 'Bitweb Main' });
});

router.get('/login', sessionChecker, function(req, res, next) {
    res.render('login/login', { title: 'Bitweb Login' });
});

router.get('/transaction', sessionChecker, function(req, res, next) {
    let userinfo = {
        userId: req.session.userId,
        sessionID: req.sessionID,
    };

    db.connectDB()
        .then(() => machUsers.getUserById(userinfo.userId))
        .then((user) => {
            console.log('result user =>', user);
            userinfo.user = user._doc;
            if(userinfo.user.etherId != undefined) {
                userinfo.etherId = userinfo.user.etherId;

                machEthers.getEtherById(userinfo.etherId).then((ethers) => {
                    userinfo.ethers = ethers._doc;
                    console.log(ethers._doc);
                    res.render('transaction/myTransaction', {title: 'DAIOS My Transaction', data: userinfo});
                });
            } else {
                res.render('transaction/myTransaction', {title: 'DAIOS My Transaction', data: userinfo});
            }
        }).catch((err) => {
        console.log('reject err=>', err);
        res.render('transaction/myTransaction', {title: 'DAIOS My Transaction', data: userinfo});
    })
});

router.get('/howtouse', function(req, res, next) {
    res.render('help/howtouse', { title: 'DAIOS Rending Page' });
});

router.get('/userinfo', sessionChecker, function(req, res, next) {
    let userinfo = {
        userId: req.session.userId,
        sessionID: req.sessionID,
    };

    db.connectDB()
        .then(() => machUsers.getUserById(userinfo.userId))
        .then((user) => {
            console.log('result user =>', user);
            userinfo.user = user._doc;
            if(userinfo.user.etherId != undefined) {
                userinfo.etherId = userinfo.user.etherId;

                machEthers.getEtherById(userinfo.etherId).then((ethers) => {
                    userinfo.ethers = ethers._doc;
                    console.log(ethers._doc);
                    res.render('user/userInformation', {title: 'DAIOS My Transaction', data: userinfo});
                });
            } else {
                res.render('user/userInformation', {title: 'DAIOS My Transaction', data: userinfo});
            }
        }).catch((err) => {
        console.log('reject err=>', err);
        res.render('user/userInformation', {title: 'DAIOS My Transaction', data: userinfo});
    })
});

router.get('/logout', function(req, res, next) {
    req.session.destroy();
    res.redirect('/login');
});

router.get('/notsales', function(req, res, next) {
    res.render('notsales', { title: 'Bitweb Main' });
});

router.get('/main', function(req, res, next) {
    res.render('index', { title: 'Bitweb Main' });
});

router.get('/header', function(req, res, next) {
    res.render('common/header', { title: 'Bitweb Header' });
});

router.get('/menu', function(req, res, next) {
    res.render('common/menu', { title: 'Bitweb menu' });
});

router.get('/footer', function(req, res, next) {
    res.render('common/footer', { title: 'Bitweb Footer' });
});

router.get('/findId', function(req, res, next) {
    res.render('login/findId', { title: 'Bitweb Find ID' });
});

router.get('/findPassword', function(req, res, next) {
    res.render('login/findPassword', { title: 'Bitweb Find Password' });
});

router.get('/signup', function(req, res, next) {
    res.render('login/register', { title: 'Bitweb sign up' });
});


router.get('/buy', function(req, res, next) {
    res.render('transaction/buyCoin', { title: 'Bitweb Buy' });
});

router.get('/search', function(req, res, next) {
    res.render('common/search', { title: 'Bitweb Search' });
});

router.get('/item/buy/list', function(req, res, next) {
    res.render('buy/list', { title: 'Bitweb Item Buy' });
});


router.get('/sample', function(req, res, next) {
    res.render('sample', { title: 'Express' });
});


function eventDay(req, res, next) {

    let currentTime = createCurrentUnixTime();
    let firstStartDay = '2018-11-12 00:00:00'
    let firstEndDay = '2018-11-20 23:59:59'

    let secondStartDay = '2018-11-21 00:00:00'
    let secondEndDay = '2018-11-28 23:59:59'

    let thirdStartDay = '2018-12-12 00:00:00'
    let thirdEndDay = '2018-12-19 23:59:59'

    console.log('currentTime        =>', currentTime)
    console.log('firstStartDay      =>', createUnixTime(firstStartDay))
    console.log('firstEndDay        =>', createUnixTime(firstEndDay))

    switch (true) {
        case ((createUnixTime(firstStartDay) <= currentTime) &&
            (createUnixTime(firstEndDay) >= currentTime)):
            console.log('event term!')
            req.session.preSalesYN =  'Y';
            next();
            break;

        case ((createUnixTime(secondStartDay) <= currentTime) &&
            (createUnixTime(secondEndDay) >= currentTime)):
            console.log('event term!')
            req.session.preSalesYN =  'Y';
            next();
            break;

        case ((createUnixTime(thirdStartDay) <= currentTime) &&
            (createUnixTime(thirdEndDay) >= currentTime)):
            console.log('event term!')
            req.session.preSalesYN =  'Y';
            next();
            break;

        default:
            req.session.preSalesYN = 'N';
            res.redirect('/notsales');
            break;
    }

}

var createCurrentUnixTime = function () {
    let koTime = new Date().toLocaleString('ko-KR').replace(/T/, ' ').replace(/\..+/, '');
    let pastDateTime = datetime.create(koTime);
    // console.log('koTime=>', koTime)
    // console.log('pastDateTime getTime=>', pastDateTime.getTime())

    return pastDateTime.getTime();
}

var createUnixTime = function (day) {
    let pastDateTime = datetime.create(day);
    return pastDateTime.getTime();
}

module.exports = router;

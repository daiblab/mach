var express = require('express');
var router = express.Router();
var controllerUsers = require('../controllers/users')
var BitwebResponse = require('../utils/BitwebResponse')

router.get('/:userId', function (req, res, next) {
    // res.send('respond with a resource');

    var bitwebResponse = new BitwebResponse();
    if (req.params.userId != null) {
        controllerUsers.get(req)
            .then(result => {
                bitwebResponse.code = 200;
                bitwebResponse.data = result;
                res.status(200).send(bitwebResponse.create())
            }).catch((err) => {
            console.error('err=>', err)
            bitwebResponse.code = 500;
            bitwebResponse.message = err;
            res.status(500).send(bitwebResponse.create())
        })
    }

});

router.post('/', function (req, res, next) {

    var bitwebResponse = new BitwebResponse();
    if (req.body.userTag != null) {

        let controllerEthers = require('../controllers/ethers');
        let controllerAgreements = require('../controllers/agreements');
        let controllerBanks = require('../controllers/banks');

        let ethers = req.body.ethers;
        let agreements = req.body.agreements;
        let banks = req.body.banks;

        controllerEthers.createByEther(ethers)
            .then(result => {
                if (result._id != null) {
                    let etherId = result._id;
                    console.log('etherId=>', etherId)
                    controllerAgreements.createByAgreement(agreements)
                        .then(result => {
                            if (result._id != null) {
                                let agreementId = result._id;
                                controllerBanks.createByBank(banks)
                                    .then(result => {
                                        if (result._id != null) {
                                            let bankId = result._id;
                                            controllerUsers.createWithIds(req, etherId, agreementId, bankId)
                                                .then(result => {

                                                    req.session.userTag = result.userTag;
                                                    req.session.userId = result._id;

                                                    bitwebResponse.code = 200;
                                                    bitwebResponse.data = result;
                                                    res.status(200).send(bitwebResponse.create())
                                                })
                                        }
                                    })
                            }
                        }).catch((err) => {
                        console.error('err=>', err)
                        bitwebResponse.code = 500;
                        bitwebResponse.message = err;
                        res.status(500).send(bitwebResponse.create())
                    })
                }
            })
    }
})

router.post('/login', function (req, res, next) {
    if (req.body.userTag != null && req.body.password != null) {

        let userTag = req.body.userTag;
        let crypto = require('crypto');
        let password = crypto.createHash('sha256').update(req.body.password).digest('base64');
        let data = {}

        data['userTag'] = userTag;
        data['password'] = password;
        console.log('data=>', data)

        var bitwebResponse = new BitwebResponse();
        controllerUsers.getUser(data)
            .then(result => {

                req.session.userTag = result.userTag;
                req.session.userId = result._id;

                let resData = {"userTag": result.userTag, "userId": result._id}
                bitwebResponse.code = 200;
                bitwebResponse.data = resData;
                res.status(200).send(bitwebResponse.create())
            }).catch((err) => {
            console.error('err=>', err)

            let resErr = "Incorrect ID or password";
            bitwebResponse.code = 400;
            bitwebResponse.message = resErr;
            res.status(400).send(bitwebResponse.create())
        })

    }
});
router.put('/:userId', function (req, res, next) {

    let controllerEthers = require('../controllers/ethers');

    var bitwebResponse = new BitwebResponse();
    if (req.params.userId != null) {
        controllerUsers.update(req)
            .then((result) => {

                let resData = result;
                let data = {}
                data['address'] = req.body.ethers.address;
                controllerEthers.updateByEtherId(req.body.etherId, data)
                    .then((result) => {

                        resData['address'] = result.address;
                        console.log('resData=>', resData);
                        bitwebResponse.code = 200;
                        bitwebResponse.data = resData;
                        res.status(200).send(bitwebResponse.create())
                    })



            }).catch((err) => {
            console.error('err=>', err)
            bitwebResponse.code = 500;
            bitwebResponse.message = err;
            res.status(500).send(bitwebResponse.create())
        })
    }
})

router.delete('/:userId', function (req, res, next) {

    var bitwebResponse = new BitwebResponse();
    if (req.params.userId != null) {
        controllerUsers.remove(req)
            .then(result => {
                bitwebResponse.code = 200;
                bitwebResponse.data = result;
                res.status(200).send(bitwebResponse.create())
            }).catch((err) => {
            console.error('err=>', err)
            bitwebResponse.code = 500;
            bitwebResponse.message = err;
            res.status(500).send(bitwebResponse.create())
        })
    }
})

module.exports = router;

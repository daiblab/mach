var Users = require('../libs/users');

function createUser (data) {
    return new Promise((resolve, reject) => {
        console.log(data)
        Users.findOneAndUpdate(
            {
                "email": data.email
            },
            {
                $set:data
            },
            {upsert: true, new: true},
            function(err, user) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                console.log('createUser done: ' + user._id)
                resolve(user)
            }
        )
    })
}

function getUserById (id) {
    return new Promise((resolve, reject) => {
        Users.findOne(
            {"_id": id},
            function(err, user) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                console.log('getUserById done: ' + user)
                resolve(user)
            }
        )
    })
}

function getUserByIdAndPassword (data) {
    return new Promise((resolve, reject) => {
        Users.findOne(
            {
                "userTag"        : data.userTag,
                "password"  : data.password
            },
            function(err, user) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                console.log('getUserById done: ' + user)
                resolve(user)
            }
        )
    })
}

function getUserByEmail (email, body) {
    return new Promise((resolve, reject) => {
        Users.findOne(
            {"email": email},
            function(err, user) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                console.log('getUserById done: ' + user)
                resolve(user)
            }
        )
    })
}

function updateUserById(userId, body) {
    return new Promise((resolve, reject) => {
        Users.findOneAndUpdate(
            {"_id": userId
            },
            {$set: body
            },
            {upsert: false, new: true},
            function(err, data) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                resolve(data)
            })
    })
}

function deleteUserById (id) {
    return new Promise((resolve, reject) => {
        Users.findByIdAndRemove(
            id,
            function(err, user) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                console.log('getUserById done: ' + user)
                resolve(user)
            }
        )
    })
}

exports.createUser = createUser;
exports.getUserById = getUserById;
exports.getUserByIdAndPassword = getUserByIdAndPassword;
exports.getUserByEmail = getUserByEmail;
exports.updateUserById = updateUserById;
exports.deleteUserById = deleteUserById;

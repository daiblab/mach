var Agreements = require('../libs/agreements');

function createAgreement (data) {
    return new Promise((resolve, reject) => {
        console.log(data)
        Agreements.findOneAndUpdate(
            {
                "use": data.use
            },
            {
                $set:data
            },
            {upsert: true, new: true},
            function(err, agreement) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                console.log('createAgreement done: ' + agreement._id)
                resolve(agreement)
            }
        )
    })
}

function getAgreementById (id) {
    return new Promise((resolve, reject) => {
        Agreements.findOne(
            {"_id": id},
            function(err, agreement) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                console.log('getEtherById done: ' + agreement)
                resolve(agreement)
            }
        )
    })
}

exports.createAgreement = createAgreement
exports.getAgreementById = getAgreementById

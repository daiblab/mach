
var db = require('../utils/db');
var bitwebUsers = require('../services/users');
var crypto = require('crypto');

function get(req) {
    return new Promise((resolve, reject) => {

        var userId = req.params.userId;
        db.connectDB()
            .then(() => bitwebUsers.getUserById(userId))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
                reject(err)
        })
    })

}

function getUser(data) {
    return new Promise((resolve, reject) => {

        db.connectDB()
            .then(() => bitwebUsers.getUserByIdAndPassword(data))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })

}

function create(req) {
    return new Promise((resolve, reject) => {

        var data = req.body;
        db.connectDB()
            .then(() => bitwebUsers.createUser(data))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })

}

function createWithIds(req, etherId, agreementId, bankId) {
    return new Promise((resolve , reject) => {

        var data = req.body;
        delete data['ethers'];
        delete data['agreements'];
        delete data['banks'];

        var password = crypto.createHash('sha256').update(req.body.password).digest('base64');
        data['password'] = password;
        data['etherId'] = etherId;
        data['agreementId'] = agreementId;
        data['bankId'] = bankId;
        data['regDate'] = new Date().toLocaleString('ko-KR').replace(/T/, ' ').replace(/\..+/, '')

        console.log('createWithIds data=>', data);

        db.connectDB()
            .then(() => bitwebUsers.createUser(data))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })
}

function update(req) {
    return new Promise((resolve , reject) => {

        var userId = req.params.userId;

        var data = {};
        data['email'] = req.body.email;

        if (req.body.password != undefined) {
            var password = crypto.createHash('sha256').update(req.body.password).digest('base64');
            data['password'] = password;
        }

        db.connectDB()
            .then(() => bitwebUsers.updateUserById(userId, data))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })
}

function remove(req) {
    return new Promise((resolve, reject) => {

        var userId = req.params.userId;
        db.connectDB()
            .then(() => bitwebUsers.deleteUserById(userId))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })

}

exports.get = get;
exports.getUser = getUser;
exports.create = create;
exports.createWithIds = createWithIds;
exports.update = update;
exports.remove = remove;


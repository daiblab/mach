var db = require('../utils/db');
var bitwebEtherHistorys = require('../services/etherhistorys');

function get(req) {
    return new Promise((resolve, reject) => {

        var historyId = req.params.historyId;
        db.connectDB()
            .then(() => bitwebEtherHistorys.getEtherHistoryById(historyId))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })

}

function create(req) {
    return new Promise((resolve, reject) => {

        var data = req.body;
        db.connectDB()
            .then(() => bitwebEtherHistorys.createEtherHistory(data))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })

}

function createEtherHistoryByEtherId(etherId , body) {
    return new Promise((resolve, reject) => {

        var data = {};
        data['coin'] = body.coin;
        data['etherId'] = etherId;
        data['regDate'] = new Date().toLocaleString('ko-KR').replace(/T/, ' ').replace(/\..+/, '')
        data['completeDate'] = "";
        data['status'] = false;
        data['bonus'] = body.bonus;
        data['mach'] = body.mach;

        db.connectDB()
            .then(() => bitwebEtherHistorys.createEtherHistory(data))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })

}

function update(req) {
    return new Promise((resolve , reject) => {

        var historyId = req.params.historyId;
        var data = req.body;
        db.connectDB()
            .then(() => bitwebEtherHistorys.updateEtherHistoryById(historyId, data))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })
}

function updateEtherHistoryStatusById(historyId) {
    return new Promise((resolve , reject) => {

        var data = {};
        data['completeDate'] = new Date().toLocaleString('ko-KR').replace(/T/, ' ').replace(/\..+/, '');
        data['status'] = true;
        db.connectDB()
            .then(() => bitwebEtherHistorys.updateEtherHistoryById(historyId, data))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })
}

exports.get = get;
exports.create = create;
exports.createEtherHistoryByEtherId = createEtherHistoryByEtherId;
exports.update = update;
exports.updateEtherHistoryStatusById = updateEtherHistoryStatusById;

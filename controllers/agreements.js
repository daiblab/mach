var db = require('../utils/db');
var bitwebAgreements = require('../services/agreements');

function get(req) {
    return new Promise((resolve, reject) => {

        var agreementId = req.params.agreementId;
        db.connectDB()
            .then(() => bitwebAgreements.getAgreementById(agreementId))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })

}

function create(req) {
    return new Promise((resolve, reject) => {

        var data = req.body;
        db.connectDB()
            .then(() => bitwebAgreements.createAgreement(data))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })

}

function createByAgreement(agreement) {
    return new Promise((resolve, reject) => {

        var data = agreement;
        db.connectDB()
            .then(() => bitwebAgreements.createAgreement(data))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })

}

exports.get = get;
exports.create = create;
exports.createByAgreement = createByAgreement;

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId;

var usersSchema = new Schema({
    userTag: String,
    password: String,
    email: String,
    active: Boolean,
    otp_auth_code: String,
    mail_auth_code: String,
    regDate: String,          //회원가입 날짜
    etherId: ObjectId,
    bankId: ObjectId,
    agreementId: ObjectId,
});

module.exports = usersSchema;

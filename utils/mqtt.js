let config = require('../config')
let mqtt = require('mqtt');
let mqtt_url = config.mqtt.url;

function publishEmail(jsonData) {
    return new Promise((resolve, reject) => {

        let client = mqtt.connect(mqtt_url);
        let data = JSON.stringify(jsonData);
        console.log('email data=>', data)

        client.on('connect', function () {
            client.subscribe('bitweb');
            client.publish('bitweb/email', data);
            client.end();
        });

        resolve(jsonData)
    });
}

function publishEtherScanSchduler(jsonData) {
    return new Promise((resolve, reject) => {

        let client = mqtt.connect(mqtt_url);
        let data = JSON.stringify(jsonData);
        console.log('jsonData=>', data)
        client.on('connect', function () {
            client.subscribe('bitweb');
            client.publish('bitweb/etherScanSchduler', data);
            client.end();
        });

        resolve(jsonData)
    });
}

function subscribeEmail() {

    let client = mqtt.connect(mqtt_url);

    client.on('connect', function () {
        client.subscribe('bitweb');
    });

    client.on('message', function (topic, message) {

        let msg = message.toString();
        console.log('msg=>', msg);
        // client.end();
    });
}

function subscribeEtherScanSchduler() {

    let client = mqtt.connect(mqtt_url);

    client.on('connect', function () {
        client.subscribe('bitweb');
    });

    client.on('message', function (topic, message) {

        let msg = message.toString();
        console.log('msg=>', msg);

        let sendJson = JSON.parse(msg)

        if (sendJson.status != undefined
            && sendJson.status == true) {

            console.log('checked etherscan API !');

            let controllerEthers = require('../controllers/ethers')
            let controllerEtherHistorys = require('../controllers/etherHistorys');
            let sumCoin = sendJson.coin + sendJson.total_coin;
            let totalCoinJson = {"total_coin": sumCoin};

            controllerEthers.updateTotalCoin(sendJson.etherId, totalCoinJson)
                .then(result => {
                    console.log('finished add coin ==>', result);
                    controllerEtherHistorys.updateEtherHistoryStatusById(sendJson.historyId)
                        .then(result => {
                            console.log('update ether history status!');
                        })

                }).catch((err) => {
                console.error('err=>', err)
            })


        }

        client.end();
    });
}

exports.publishEmail = publishEmail;
exports.publishEtherScanSchduler = publishEtherScanSchduler;
exports.subscribeEmail = subscribeEmail;
exports.subscribeEtherScanSchduler = subscribeEtherScanSchduler;


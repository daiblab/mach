var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var session = require('express-session');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var ethersRouter = require('./routes/ethers');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.engine('.html', require('ejs').__express);
app.set('view engine', 'html');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

// app.use(passport.session());

app.use(session({
    secret: 'bitweb123', //Only enable https
    name: 'bitweb_sid',
    // store: new MongoStore({ url: DB_URI}), // connect-mongo session store
    proxy: false,
    resave: true,
    saveUninitialized: true,
    cookie: {
        // maxAge: 60 * 60 * 24 * 30 * 10000 // 쿠키 유효기간 하루 (24시간) * 30일 //현재 무기한
        expires: 60 * 60 * 24 * 30 * 10000 // 쿠키 유효기간 하루 (24시간) * 30일 //현재 무기한
    }
}));

// app.use((req, res, next) => {
//     if (req.cookies.bitweb_sid && !req.session.id) {
//         res.clearCookie('bitweb_sid');
//     }
//     next();
// });

/**
 * 외부 라이브러리는 가급적 npm으로 install 한 후, app.use를 사용하여 라이브러리를 include한다.
 * 현재 사용중인 외부 라이브러리 : bootstrap, chart.js
 */
// use bootstrap
app.use('/bootstrap', express.static(__dirname + '/node_modules/bootstrap/dist/js')); // redirect bootstrap JS
app.use('/jquery', express.static(__dirname + '/node_modules/jquery/dist')); // redirect JS jQuery
app.use('/bootstrap_css', express.static(__dirname + '/node_modules/bootstrap/dist/css')); // redirect CSS bootstrap

// use chart.js
app.use('/chart',express.static(__dirname + '/node_modules/chart.js/dist'));

// use font-awesome
app.use('/font-awesome', express.static(__dirname + '/node_modules/@fortawesome/fontawesome-free/css')); // redirect CSS font-awesome
app.use('/webfonts', express.static(__dirname + '/node_modules/@fortawesome/fontawesome-free/webfonts')); // redirect CSS font-awesome

app.use('/blockchain', express.static(__dirname + '/blockchain')); // redirect JS jQuery

app.use('/', indexRouter);
app.use('/sample', indexRouter);
app.use('/users', usersRouter);
app.use('/ethers', ethersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
